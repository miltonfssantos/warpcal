#!/usr/bin/env python3

# Parallel lines - Warping Calibration
# 1. Select corners
# 2. Select fixed corners (2)

from tkinter import *
from os import listdir
from os import path
from PIL import Image, ImageTk
from time import sleep
import glob
import json
import sys, getopt


## Init
X = 640                           # Canvas width
Y = 582                            # Canvas height
x0 = 0
y0 = 0
Number = -1                      # Starting file in List
Drawing = 0
X0_Rec = 0
Y0_Rec = 0
RecTag = 'none'
RecTagLast = 'Rec'+str(0)
RecLib = {}
TagText = 'Bem vindo de volta!!'   # Starting TagText
ClickStatusMessage = 'Clique inativo. Ative a janela!'
Click= 0                           # Starting mouse status
MouseP = 'Mouse inativo'           # Starting mouse dialog
List = ''
FramePath = 'C:/Users/administrator/Documents/frames'
norec=0
firstframes=0

# global starting Lane
# 0 = left || 1 = right
Lane = 0
if Lane == 0:
    Lside = 'left'
else:
    LSide = 'right'

def NextPoint():
    global RecTag
    if RecTag == 'none':
        if RecLib.__len__() > 0:
            list(RecLib).sort()
            RecTag = list(RecLib)[0]
            Quadro.create_rectangle(int(round(RecLib[RecTag][1]*X/x0)),
                                    int(round(RecLib[RecTag][2]*Y/y0)),
                                    int(round(RecLib[RecTag][3]*X/x0)),
                                    int(round(RecLib[RecTag][4]*Y/y0)),
                                    width=3, outline="red")
    else:
        Quadro.create_rectangle(int(round(RecLib[RecTag][1]*X/x0)),
                                int(round(RecLib[RecTag][2]*Y/y0)),
                                int(round(RecLib[RecTag][3]*X/x0)),
                                int(round(RecLib[RecTag][4]*Y/y0)),
                                width=3, outline="black")
        new_idx=list(RecLib).index(RecTag)+1
        if new_idx >= list(RecLib).__len__():
            new_idx = 0
        RecTag=list(RecLib)[new_idx]
        Quadro.create_rectangle(int(round(RecLib[RecTag][1]*X/x0)),
                                int(round(RecLib[RecTag][2]*Y/y0)),
                                int(round(RecLib[RecTag][3]*X/x0)),
                                int(round(RecLib[RecTag][4]*Y/y0)),
                                width=3, outline="red")

def PriorImage(event=None):
    global Number, RecTag, RecLib, FramePath, img, imagem, imagesprite, x0, y0
    RecLib = {}
    RecTag='none'
    N=Number
    Number = N-1
    if Number < 0:
        TagText = 'Fim da sequencia!'
        Tag.set(TagText)
        Number = 0
        TagText = ("Quadro: "+str(List[Number][framepathsize:]))
        Tag.set(TagText)
        img = Image.open(List[Number])
        x0, y0 = img.size
        resizImg = img.resize((X,Y),Image.ANTIALIAS)
        imagem = ImageTk.PhotoImage(resizImg)
        imagesprite = Quadro.create_image(X/2,Y/2,image=imagem)
        Quadro.update_idletasks()
        # with open(List[Number][:-4]+'.json', 'r') as fp:
        #     RecLib = json.load(fp)
        #     fp.close()
        #     for idx in list(RecLib.items()):
        #         Quadro.create_rectangle(int(round(RecLib[idx[0]][1]*X/x0)),
        #                                 int(round(RecLib[idx[0]][2]*Y/y0)),
        #                                 int(round(RecLib[idx[0]][3]*X/x0)),
        #                                 int(round(RecLib[idx[0]][4]*Y/y0)),
        #                                 width=3, activeoutline="red")

    else:
        TagText = ("Quadro: "+str(List[Number][framepathsize:]))
        Tag.set(TagText)
        img = Image.open(List[Number])
        x0, y0 = img.size
        resizImg = img.resize((X,Y),Image.ANTIALIAS)
        imagem = ImageTk.PhotoImage(resizImg)
        imagesprite = Quadro.create_image(X/2,Y/2,image=imagem)
        Quadro.update_idletasks()
        # with open(List[Number][:-4]+'.json', 'r') as fp:
        #     RecLib = json.load(fp)
        #     fp.close()
        #     for idx in list(RecLib.items()):
        #         Quadro.create_rectangle(int(round(RecLib[idx[0]][1]*X/x0)),
        #                                 int(round(RecLib[idx[0]][2]*Y/y0)),
        #                                 int(round(RecLib[idx[0]][3]*X/x0)),
        #                                 int(round(RecLib[idx[0]][4]*Y/y0)),
        #                                 width=3, activeoutline="red")

def NextImage(event=None):
    global Number, RecTag, RecLib,  FramePath, img, imagem, imagesprite, x0, y0
    RecLib = {}
    RecTag = 'none'
    N=Number
    Number = N+1
    if Number > len(List):
        TagText = 'Fim da sequência!'
        Tag.set(TagText)
        Number = len(List)
        TagText = ("Quadro: "+str(List[Number][framepathsize:]))
        Tag.set(TagText)
        img = Image.open(List[Number])
        x0, y0 = img.size
        resizImg = img.resize((X,Y),Image.ANTIALIAS)
        imagem = ImageTk.PhotoImage(resizImg)
        imagesprite = Quadro.create_image(X/2,Y/2,image=imagem)
        Quadro.update_idletasks()
        # with open(List[Number][:-4]+'.json', 'r') as fp:
        #     RecLib = json.load(fp)
        #     fp.close()
        #     for idx in list(RecLib.items()):
        #         Quadro.create_rectangle(int(round(RecLib[idx[0]][1]*X/x0)),
        #                                 int(round(RecLib[idx[0]][2]*Y/y0)),
        #                                 int(round(RecLib[idx[0]][3]*X/x0)),
        #                                 int(round(RecLib[idx[0]][4]*Y/y0)),
        #                                 width=3, activeoutline="red")
    else:
        TagText = ("Quadro: "+str(List[Number][framepathsize:]))
        Tag.set(TagText)
        img = Image.open(List[Number])
        x0, y0 = img.size
        resizImg = img.resize((X,Y),Image.ANTIALIAS)
        imagem = ImageTk.PhotoImage(resizImg)
        imagesprite = Quadro.create_image(X/2,Y/2,image=imagem)
        Quadro.update_idletasks()
        # with open(List[Number][:-4]+'.json', 'r') as fp:
        #     RecLib = json.load(fp)
        #     fp.close()
        #     for idx in list(RecLib.items()):
        #         Quadro.create_rectangle(int(round(RecLib[idx[0]][1]*X/x0)),
        #                                 int(round(RecLib[idx[0]][2]*Y/y0)),
        #                                 int(round(RecLib[idx[0]][3]*X/x0)),
        #                                 int(round(RecLib[idx[0]][4]*Y/y0)),
        #                                 width=3, activeoutline="red")

def MouseXY(MouseLoc):
    MouseP = (
            'Mouse em '
            +str(MouseLoc.x)+'x'+str(MouseLoc.y)
            +' do Canvas, '
            +str(int(round((MouseLoc.x)*x0/X)))+'x'
            +str(int(round((MouseLoc.y)*y0/Y)))
            +' da imagem.'
            )
    MousePosition.set(MouseP)

def MouseOUT(MouseLoc):
    MouseP = 'Mouse está fora do Canvas'
    MousePosition.set(MouseP)

def MouseCLICK1(MouseLoc):
    global Drawing, X0_Rec, Y0_Rec, RecLib, norec
    if norec==0:
        if Drawing == 0: # se Drawing=0
            Drawing = 1 # vamos subir a flag de desenho
            X0_tmp = MouseLoc.x #capturar o x inicial
            if X0_tmp < round(X/2):
                X0_Rec = 0 #capturar o x inicial
            else:
                X0_Rec = round(X/2)
            Y0_Rec = MouseLoc.y #capturar o y inicial

            ClickStatusMessage = 'Desenhando. Clique para gerar a caixa.'
            ClickStatus.set(ClickStatusMessage) # e informar o usuário
        else: # mas se a bandeira de desenho já estiver levantada,
            Drawing = 0 # vamos abaixar a bandeira
            X1_tmp = MouseLoc.x #capturar o x final
            if X1_tmp < round(X/2):
                X1_Rec = round(X/2) #capturar o x inicial
            else:
                X1_Rec = X-1
            Y1_Rec = MouseLoc.y #capturar o y final
            sortedList=sorted(list(RecLib))
            if int(list(RecLib).__len__())==0:
                newItem=0
            else:
                lastItem=sortedList[int(list(RecLib).__len__())-1]
                newItem=int(lastItem[3:])+1
            # RecLib['Rec'+str(newItem)] = [Quadro.create_rectangle(X0_Rec, Y0_Rec,
            #       X1_Rec, Y1_Rec, width=3, activeoutline="red"), #criar um retângulo
            #       int(round(X0_Rec*x0/X)), int(round(Y0_Rec*y0/Y)),
            #       int(round(X1_Rec*x0/X)), int(round(Y1_Rec*y0/Y))]
            # # e gravar o ID do objeto e suas coordenadas numa biblioteca
            ClickStatusMessage = ('Empacotador pronto. '
                                  'Clique para começar a encaixar!')
            ClickStatus.set(ClickStatusMessage)
            # with open(List[Number][:-4]+'.json', 'w') as fp:
            #     json.dump(RecLib, fp)
            #     fp.close()

# def DeleteRectangle():
#     global RecLib, RecTag, Quadro, norec
#     if norec==0:
#         Quadro.delete(RecLib[RecTag])# [0])
#         #Quadro.update()
#         Quadro.update_idletasks()
#         del RecLib[RecTag]
#         RecTag='none'
#         with open(List[Number][:-4]+'.json', 'w') as fp:
#             json.dump(RecLib, fp)
#             fp.close()
#         for idx in list(RecLib.items()):
#             Quadro.create_rectangle(int(round(RecLib[idx[0]][1]*X/x0)),
#                                     int(round(RecLib[idx[0]][2]*Y/y0)),
#                                     int(round(RecLib[idx[0]][3]*X/x0)),
#                                     int(round(RecLib[idx[0]][4]*Y/y0)),
#                                        width=3, outline="black", activeoutline="red")

def MouseCLICK3(MouseLoc):
    ClickStatusMessage = 'Mouse3!'
    ClickStatus.set(ClickStatusMessage)

def main(argv):
    global FramePath, norec, firstframes
    try:
        opts, args = getopt.getopt(argv,"hi:nf",["framepath="])
    except getopt.GetoptError:
        print('===================================================================================================')
        print('USAGE: ./hfcd.py -i <Frames Path> [options]')
        print('===================================================================================================')
        print('<Frames Path> = Folder with pictures and JSON')
        print('-----')
        print('[options]')
        print('-n = no record enable')
        print('-f = show first frames on start')
        print('===================================================================================================')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('===================================================================================================')
            print('USAGE: ./hfcd.py -i <Frames Path> [options]')
            print('===================================================================================================')
            print('<Frames Path> = Folder with pictures and JSON')
            print('-----')
            print('[options]')
            print('-n = no record enable')
            print('-f = show first frames on start')
            print('===================================================================================================')
            sys.exit()
        elif opt in ("-i", "--framepath"):
            FramePath = arg
        elif opt in ("-n", "--norec"):
            norec = 1
        elif opt in ("-f", "--first"):
            firstframes = 1
    print('Input Frames Path is %s' % (FramePath))

if __name__ == "__main__":
   if len(sys.argv) > 1:
      #print("%d" % (len(sys.argv)))
      main(sys.argv[1:])
   else:
      print('===================================================================================================')
      print('USAGE: ./hfcd.py -i <Frames Path> [options]')
      print('===================================================================================================')
      print('<Frames Path> = Folder with pictures and JSON')
      print('-----')
      print('[options]')
      print('-n = no record enable')
      print('-f = show first frames on start')
      print('===================================================================================================')

## Criar a lista de arquivos
if FramePath != '':
    framepathsize=FramePath.__len__()-1
    #List = listdir(FramePath)
    List = glob.glob(FramePath+"*.jpg")
    # Listjson = glob.glob(FramePath+"*.json")
    # if firstframes==0:
    #     Number=len(Listjson)-1
    List.sort()

## Criar o splash
#SPLASH = 'C:/Users/administrator/Documents/human_crop_failures_detector/splash.png'
SPLASH = './SV_splash.png'
img = Image.open(SPLASH)
resizImg = img #.resize((X,Y),Image.ANTIALIAS)

## Iniciar a janela
Janela = Tk()

Tag = StringVar()
MousePosition = StringVar()
ClickStatus = StringVar()
Rexx = StringVar()

Janela.title("SensorVision Ltda.")
Janela.geometry(str(X+30)+'x'+str(Y+90))

## create a ToolBar
ToolBar = Frame(Janela)

b1 = Button(ToolBar, text="<< imagem", width=15, command=PriorImage)
b1.pack(side=LEFT, padx=2, pady=2)

b2 = Button(ToolBar, text="imagem >>", width=15, command=NextImage)
b2.pack(side=LEFT, padx=2, pady=2)

# b3 = Button(ToolBar, text="proximo retângulo", width=15, command=NextRectangle)
# b3.pack(side=LEFT, padx=2, pady=2)
#
# b4 = Button(ToolBar, text="deletar retângulo", width=15, command=DeleteRectangle)
# b4.pack(side=LEFT, padx=2, pady=2)

ToolBar.pack()

separator = Frame(height=2, bd=1, relief=SUNKEN)
separator.pack( padx=5, pady=5)

Quadro = Canvas(Janela, width=X, height=Y, cursor='crosshair')
Quadro.pack()
imagem = ImageTk.PhotoImage(resizImg)
imagesprite = Quadro.create_image(X/2,Y/2,image=imagem)

# Quadro & Janela bindings
Quadro.bind("<Motion>", MouseXY)
Quadro.bind("<Leave>", MouseOUT)
Quadro.bind("<Button-1>", MouseCLICK1)
Quadro.bind("<Button-3>", MouseCLICK3)
Janela.bind("a", PriorImage)
Janela.bind("d", NextImage)

separator = Frame(height=2, bd=1, relief=SUNKEN)
separator.pack( padx=5, pady=5)

## create a StatusBar
StatusBar = Frame(Janela)

Tag.set(TagText)
TextTag = Label(StatusBar, textvariable=Tag, width=50)
TextTag.grid(row=0, column=1)

MousePosition.set(MouseP)
XYindicator = Label(StatusBar, textvariable=MousePosition, width=50)
XYindicator.grid(row=0, column=2)

ClickStatus.set(ClickStatusMessage)
MouseStatus = Label(StatusBar, textvariable=ClickStatus, width=50)
MouseStatus.grid(row=0, column=3)

StatusBar.pack()

mainloop()
